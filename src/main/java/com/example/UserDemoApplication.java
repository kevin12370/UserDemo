package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class UserDemoApplication {

	@GetMapping(value = "/demo")
	public String demo(){
		return "hello world";
	}

	@GetMapping(value = "/index")
	public String index(){

		return "this is index page";
	}
	public static void main(String[] args) {
		SpringApplication.run(UserDemoApplication.class, args);
	}
}
